<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Loan;
use App\Models\LoanPayment;

class LoanTest extends TestCase
{
    public function testRequiresLoanAmount()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload =  [];

        $this->json('POST', '/api/apply-for-loan', $payload, $headers)
            ->assertStatus(422)
            ->assertJson([
                "message"=> "The given data was invalid.",
                "errors"=> [
                    "loan_amount"=> [
                        "The loan amount field is required."
                    ],
                    "loan_term"=> [
                        "The loan term field is required."
                    ]
                ]
            ]);
    }

    public function testsApplyLoanCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload =  Loan::factory()->definition();

        $this->json('POST', '/api/apply-for-loan', $payload, $headers)
            ->assertStatus(201)
            ->assertJsonStructure([
                    'id',
                    'loan_amount',
                    'loan_term'
            ]);
    }

    public function testsLoanUpdatedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loan = Loan::factory()->create();

        $payload = [
            'status' => 'approved',
        ];

        $response = $this->json('PUT', '/api/loans/' . $loan->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                    'id',
                    'loan_amount',
                    'loan_term',
                    'status',
            ]);
    }

    public function testsLoanUpdatedNotValid()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loanId = 123456;
        $payload = [
            'status' => 'approved',
        ];

        $this->json('PUT', '/api/loans/'. $loanId, $payload, $headers)
        ->assertStatus(404)
        ->assertJson([
            "error"=> "Resource not found",
        ]);
    }


    public function testRequiresAmountToPay()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loan = Loan::factory()->create([
            'status' => 'approved',
        ]);
        $payload = [];

        $this->json('POST', '/api/make-loan-payment/'. $loan->id, $payload, $headers)
            ->assertStatus(422)
            ->assertJson([
                "message"=> "The given data was invalid.",
                "errors"=> [
                    "amount"=> [
                        "The amount field is required."
                    ]
                ]
            ]);
    }

    public function testsMakeLoanPaymentCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loan = Loan::factory()->create([
            'status' => 'approved',
        ]);
        $payload =  LoanPayment::factory()->definition();

        $this->json('POST', '/api/make-loan-payment/'. $loan->id, $payload, $headers)
            ->assertStatus(201)
            ->assertJsonStructure([
                    'id',
                    'amount'
            ]);
    }

    public function testsMakeLoanPaymentNotApproved()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loan = Loan::factory()->create();
        $payload =  LoanPayment::factory()->definition();

        $this->json('POST', '/api/make-loan-payment/'. $loan->id, $payload, $headers)
        ->assertStatus(422)
        ->assertJson([
            "message"=> "The given loan is not approved.",
        ]);
    }

    public function testsMakeLoanPaymentNotValid()
    {
        $user = User::factory()->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $loanId = 123456;
        $payload =  LoanPayment::factory()->definition();

        $this->json('POST', '/api/make-loan-payment/'. $loanId, $payload, $headers)
        ->assertStatus(404)
        ->assertJson([
            "error"=> "Resource not found",
        ]);
    }
}
