<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanPayment;

class LoanController extends Controller
{
   
    /**
     * List of loans applied by authenticated user
     */
    public function userLoans(Request $request)
    {
        $userId = $request->user()->id;
        return Loan::where('user_id',$userId)->get();
    }
 
    /**
     * View details of loan by id
     */
    public function show(Loan $loan)
    {
        return $loan;
    }
    
    /**
     * Apply for loan as authenticated user
     * Required two inputs loan_amount and loan_term
     * loan_term is an integer represents no of weeks
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'loan_amount' => 'required',
            'loan_term' => 'required',
        ]);

        $emi = $request->loan_amount/$request->loan_term;
        $userId = $request->user()->id;
        $status = 'approved'; //usually it should be pending and approved by admin
        $loan = Loan::create([
            'loan_amount' => $request->loan_amount,
            'loan_term' => $request->loan_term,
            'emi_amount' => $emi,
            'status' => $approved,
            'user_id'=>$userId
          ]);

        return response()->json($loan, 201);
    }

    /**
     * update loan useful for updating status or other details by admin
     */
    public function update(Request $request,Loan $loan)
    {
        $loan->update($request->all());

        return response()->json($loan, 200);
    }

    /**
     * Delete loan by id
     */

    public function delete(Request $request,Loan $loan)
    {
        $loan->delete();

         return response()->json(null, 204);
    }

    /**
     * Make payment for approved loan as authenticated user
     */
    public function makePayment(Request $request,Loan $loan)
    {
        $this->validate($request, [
            'amount' => 'required',
        ]);

        if($loan->status!='approved'){
            return response()->json(['message' => 'The given loan is not approved.'], 422);
        }
        $userId = $request->user()->id;
        $today = \Carbon\Carbon::now()->format('Y-m-d');
        $loan = LoanPayment::create([
            'amount' => $loan->emi_amount,
            'payment_date' => $today,
            'user_id'=>$userId,
            'loan_id'=>$loan->id
          ]);

        return response()->json($loan, 201);
    }
}
