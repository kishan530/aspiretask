<?php

namespace Database\Factories;

use App\Models\Loan;
use App\Models\LoanPayment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LoanPaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Loan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->numberBetween($min = 1000, $max = 1000000),
            'payment_date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'user_id'=>function () {
                return User::factory()->create()->id;
            },
            'loan_id'=>function () {
                return Loan::factory()->create()->id;
            },
        ];
    }
}
