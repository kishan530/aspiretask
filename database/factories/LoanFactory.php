<?php

namespace Database\Factories;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LoanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Loan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'loan_amount' => $this->faker->numberBetween($min = 1000, $max = 1000000),
            'loan_term' => $this->faker->numberBetween($min = 1, $max = 100),
            'emi_amount' => $this->faker->numberBetween($min = 1, $max = 100),
            'status'=>'pending',
            'user_id'=>function () {
                return User::factory()->create()->id;
            },
        ];
    }
}
