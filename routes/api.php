<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('user-loans', 'LoanController@userLoans');
    Route::get('loans/{loan}', 'LoanController@show');
    Route::post('apply-for-loan', 'LoanController@store');
    Route::put('loans/{loan}', 'LoanController@update');
    Route::delete('loans/{loan}', 'LoanController@delete');
    Route::post('make-loan-payment/{loan}', 'LoanController@makepayment');
});
