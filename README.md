This is task given by aspire

steps to run this project

Step 1:

git clone https://gitlab.com/kishan530/aspiretask.git


Step 2:

Run following commands

cd aspireTask

composer install


Step 3:

Create new database "aspire_task" in your local

Step 4:

Rename .env.example as .env with bellow command

sudo cp .env .env.example

update following details in .env file

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=aspire_task

DB_USERNAME=root

DB_PASSWORD=


Step 5:

Run following command to update your database

php artisan migrate


Step 6:

Run following command to generate keys

php artisan key:generate


Step 7:

Run following command to test application

Composer test 


Step 8:

Run following command to start application

php artisan serve

